#ifndef RUN_INFO_H
#define RUN_INFO_H

#include "constants.h"

struct timing
{
    int seconds;
    int minutes;
    int hours;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct info_about_run
{
    int number;
    person runner;
    timing start;
    timing finish;
    char club[MAX_STRING_SIZE];
};

#endif