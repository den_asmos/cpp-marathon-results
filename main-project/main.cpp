#include <iostream>
#include <iomanip>

using namespace std;

#include "run_info.h"
#include "file_runner.h"
#include "constants.h"
#include "filter.h"

void output(info_about_run* info)
{
    cout << "�����:..........: ";
    cout << info->number << '\n';
    
    cout << "��������........: ";
    cout << info->runner.last_name << " ";
    cout << info->runner.first_name[0] << ". ";
    cout << info->runner.middle_name[0] << ".";
    cout << '\n';

    cout << "���������.......: ";
    cout << setw(2) << setfill('0') << hours(info->start.hours, info->finish.hours, info->start.minutes, info->finish.minutes) << ':';
    cout << setw(2) << setfill('0') << minutes(info->start.minutes, info->finish.minutes, info->start.seconds, info->finish.seconds) << ':';
    cout << setw(2) << setfill('0') << seconds(info->start.seconds, info->finish.seconds);
    cout << '\n';

    cout << "����............: ";
    cout << info->club;
    cout << '\n';
    cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �1. ���������� ��������\n";
    cout << "�����: ������� �������\n";
    cout << "������: 12\n\n";

    info_about_run* info[MAX_FILE_ROWS_COUNT];
    int size;
    cout << "***** ���������� �������� *****\n\n";

    try
    {
        read("data.txt", info, size);

        for (int i = 0; i < size; i++)
        {
            output(info[i]);
        }

        bool (*check_function_1)(info_about_run*) = NULL;
        info_about_run** (*check_method_of_sorting)(info_about_run * array[], int size, int (*check)(info_about_run * first, info_about_run * second)) = 0;
        int (*check_criteria_of_sorting)(info_about_run * first, info_about_run * second) = 0;


        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ������� ���� ���������� � �� ���������� �� ����� ��������\n";
        cout << "2) ������� ���� ����������, � ������� ��������� ����� ��� 2:50:00\n";
        cout << "3) ����� � �������������� ����������\n";
        cout << "\n������� ����� ���������� ������: ";
        
        int item;
        cin >> item;
        cout << '\n';
        
        switch (item)
        {
        case 1:
            check_function_1 = check_run_info_by_club;
            cout << "**** ��� ��������� �� ����� �������� � �� ���������� ****\n\n";
            break;

        case 2:
            check_function_1 = check_run_info_by_timing;
            cout << "**** ��� ���������, � ������� ��������� ����� ��� 2:50:00 ****\n\n";
            break;

        case 3:
            cout << "�������� ����� ����������:\n";
            cout << "1) Bubble sort\n";
            cout << "2) Quick sort\n";
            cout << "\n������� ����� ���������� ������: ";
            int method;
            cin >> method;
            cout << '\n';
            switch (method)
            {
            case 1:
                check_method_of_sorting = bubble_sort;
                break;

            case 2:
                check_method_of_sorting = quick_sort;
                break;

            default:
                throw "������������ ����� ������";
            }

            cout << "�������� �������� ����������:\n";
            cout << "1) �� ����������� ������� ������\n";
            cout << "2) �� ����������� �������� ����������� �����, � � ������ ������ ����� �� ����������� ������� ���������\n";
            cout << "\n������� ����� ���������� ������: ";
            int criteria;
            cin >> criteria;
            cout << '\n';
            switch (criteria)
            {
            case 1:
                check_criteria_of_sorting = compare_1;
                break;

            case 2:
                check_criteria_of_sorting = compare_2;
                break;

            default:
                throw "������������ ����� ������";
            }
            break;


        default:
            throw "������������ ����� ������";
        }

        if (check_function_1)
        {
            int new_size;
            info_about_run** filtered = filter(info, size, check_function_1, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        else
        {
            check_method_of_sorting(info, size, check_criteria_of_sorting);
            for (int i = 0; i < size; i++)
            {
                output(info[i]);
            }
        }

        for (int i = 0; i < size; i++)
        {
            delete info[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }

    return 0;
}