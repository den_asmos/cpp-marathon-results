#include "filter.h"
#include "run_info.h"
#include <cstring>
#include <iostream>
#include <iomanip>

int hours(int a, int b, int c, int d)
{
	if (c < d)
	{
		return b - a;
	}
	else
	{
		return b - a - 1;
	}
}

int minutes(int a, int b, int c, int d)
{
	if (c < d)
	{
		if (a < b)
		{
			return b - a;
		}
		else
		{
			return 60 - (a - b);
		}
	}
	else
	{
		if (a < b)
		{
			return b - a - 1;
		}
		else
		{
			return 60 - (a - b) - 1;
		}
	}
}

int seconds(int a, int b)
{
	return (a < b ? b - a : 60 - (a - b));
}

info_about_run** filter(info_about_run* array[], int size, bool (*check)(info_about_run* element), int& result_size)
{
	info_about_run** result = new info_about_run * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_run_info_by_club(info_about_run* element)
{
	return strcmp(element->club, "�������") == 0;
}

bool check_run_info_by_timing(info_about_run* element)
{
	return (hours(element->start.hours, element->finish.hours, element->start.minutes, element->finish.minutes) * 60 + minutes(element->start.minutes, element->finish.minutes, element->start.seconds, element->finish.seconds) + seconds(element->finish.seconds, element->start.seconds) / 60) < 170;
}

int compare_1(info_about_run* first, info_about_run* second)
{
	int one = hours(first->start.hours, first->finish.hours, first->start.minutes, first->finish.minutes) * 60 + minutes(first->start.minutes, first->finish.minutes, first->start.seconds, first->finish.seconds) + seconds(first->finish.seconds, first->start.seconds) / 60;
	int two = hours(second->start.hours, second->finish.hours, second->start.minutes, second->finish.minutes) * 60 + minutes(second->start.minutes, second->finish.minutes, second->start.seconds, second->finish.seconds) + seconds(second->finish.seconds, second->start.seconds) / 60;
	if (one == two)
	{
		return 0;
	}
	else if (one < two)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

int compare_2(info_about_run* first, info_about_run* second)
{
	if (first->club[0] == second->club[0])
	{
		if (first->runner.last_name[0] == second->runner.last_name[0])
		{
			return 0;
		}
		else if (first->runner.last_name[0] < second->runner.last_name[0])
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	else if (first->club[0] < second->club[0])
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

info_about_run** bubble_sort(info_about_run* array[], int size, int (*check)(info_about_run* first, info_about_run* second))
{
	info_about_run* temp = new info_about_run[size];
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (check(array[j], array[j + 1]) == 1)
			{
				temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
	return array;
}

int partition(info_about_run* array[], int left, int right, int size, int (*check)(info_about_run* first, info_about_run* second))
{
	info_about_run* element = new info_about_run[size];
	info_about_run* temp = new info_about_run[size];
	int el_index = left;

	element = array[right];

	for (int i = left; i < right; i++)
	{
		if (check(array[i], element) == -1 || check(array[i], element) == 0)
		{
			temp = array[i];
			array[i] = array[el_index];
			array[el_index] = temp;
			el_index++;
		}
	}

	temp = array[el_index];
	array[el_index] = array[right];
	array[right] = temp;

	return el_index;
}

void inside_sort(info_about_run* array[], int left, int right, int size, int(*check)(info_about_run* first, info_about_run* second))
{
	if (left >= right)
	{
		return;
	}

	int index = partition(array, left, right, size, check);

	inside_sort(array, left, index - 1, size, check);
	inside_sort(array, index + 1, right, size, check);

}

info_about_run** quick_sort(info_about_run* array[], int size, int (*check)(info_about_run* first, info_about_run* second))
{
	inside_sort(array, 0, size - 1, size, check);

	return array;
}