#ifndef PROCESSING_H
#define PROCESSING_H

#include "run_info.h"
#include "file_runner.h"

void read(const char* file_name, info_about_run* array[], int& size);

#endif