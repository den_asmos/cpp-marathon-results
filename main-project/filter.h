#ifndef FILTER_H
#define FILTER_H

#include "run_info.h"

info_about_run** filter(info_about_run* array[], int size, bool (*check)(info_about_run* element), int& result_size);

bool check_run_info_by_club(info_about_run* element);

bool check_run_info_by_timing(info_about_run* element);

info_about_run** quick_sort(info_about_run* array[], int size, int (*check)(info_about_run* first, info_about_run* second));

info_about_run** bubble_sort(info_about_run* array[], int size, int (*check)(info_about_run* first, info_about_run* second));

int compare_1(info_about_run* first, info_about_run* second);

int compare_2(info_about_run* first, info_about_run* second);

int hours(int a, int b, int c, int d);

int minutes(int a, int b, int c, int d);

int seconds(int a, int b);
#endif